# Sujet

Ceci est un dépot public du test technique qui m'a été demandé dans le processus de recrutement d'Agily ([Sujet](https://gitlab.com/agily/test-technique-fullstack)).

# Présentation

Ce dépot est séparé en deux sous dossiers contenant les deux exercices demandés dans la consigne : [exercice 1](https://gitlab.com/Ar53N1K/agily-test-technique/-/tree/main/exercice1) et [exercice 2](https://gitlab.com/Ar53N1K/agily-test-technique/-/tree/main/exercice2).


# Exercice 1 : Frontend [Sujet B]


## Setup

Cet exercice est un projet NuxtJS, il répond à une architecture NuxtJS classique. Il nécessite également d'exporter une clé d'API dans l'environnement du serveur pour profiter des services de celui-ci.

```bash
# setup weather service API
$ export WS_APIKEY=<yourKey>

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

## Implementation de l'exercice

Ce premier exercice est un exercice purement frontend qui va principalement s'appuyer sur le designe de l'application et la consommation de divers API.

### Déroulement

- Documentation Javascript, Vue.js, Nuxt.js, HTML, CSS
- Initialisation du projet, création des pages et des layouts;
- Création des composents de base des pages
- Implémentation de l'API OpenWeatherMap, puis WeatherStack, puis retour sur OpenWeatherMap (cf Problèmes rencontrés)
- Design propre des pages
- Ajout de l'image de la ville grâce à FickrAPI

J'ai mis une quinzaine d'heures afin de réaliser cet exercice. La majorité étant dédiée à de la documentation et à une guerre acharnée avec le CSS; sans connaître les mots clés c'est un peu la guerre (c'était beaucoup plus fluide à la fin).

Cela était mon tout premier projet web, je n'avais jamais touché à Javascript, Nuxt, CSS et HTML auparavant.
Je pense que je ne mettrais pas plus de 7h si j'étais ammené à refaire ce sujet maintenant.

## Problèmes rencontrés

- Le partage du gitlab du sujet et et le mail indiquant quel exercice je devait réalisé m'ayant été communiqués sur deux adresses mail différentes, je me suis rencu compte après la première soirée de travail que je ne réalisait pas le bon sujet. Je suis donc repartit de cette base pour faire le sujet B.
- L'API du sujet B ([WeatherStack](https://weatherstack.com/)) nécessite un plan premium afin de pouvoir faire des requêtes sur les 7 prochins jours (609 : Weather forecast data is not supported on the current subscription plan.). Après avoir essayer d'appeler les contacts mis à disposition sur le sujet sans réponse, j'ai pris la décision d'utiliser l'API du sujet A ([OpenWeather](https://openweathermap.org/api)).
- Le dépot sujet n'était plus accessible à partir de dimanche matin.


## Pistes d'améliorations

- Continuer l'apprentissage de JavaScript, NuxtJS
- Gérer l'activation des cartes de météo sur la page /weather pour afficher les donnée détaillées des journées lors du click de l'utilisateur
- Utiliser Sass pour le CSS, je n'ai pas osé l'implémenter sur cet exercice car étant mon premier projet web j'ai préféré rester sur un fonctionnement basique mais fonctionnel
- Utiliser TypeScript, la raison est la même que pour Sass
- Faire des tests et une GitLabCI
- Nettoyer mon code, je n'ai pas eu le temps ici mais il y a des choses qui pourraient être plus propres. Notamment des morceaux de code que j'ai réalisé au début du projet et dont j'ai une bien meilleure compréhension maintenant.


# Exercice 2 : Backend

## Setup

```bash
# setup weather service API
$ export WS_APIKEY=<yourKey>

# install dependencies
$ npm install

# run server
$ node app.js
```

## Implementation de l'exercice

Le but de cet exercice est de faire un backend pour les consommations d'API du front. Pour cela, j'utiliserais le framework Express.js et NodeCache comme spécifié dans le sujet.

### Déroulement
- Documentation Express.js et NodeCache
- Initialisation du projet 
- Création des endpoints (/geo et /weather)
- Tests avec Postman

Cet exercice m'a pris environ 3h, en effet j'avais déjà eu une expérience de backend en Java par le passé et Express et NodeCache sont plutôt simples d'utilisation et bien documentés.

## Pistes d'améliorations
- Nettoyer la data récupérée dans /weather
- Lier mon front et mon back, cela n'a pas été fait par manque de temps
- Faire un endpoint pour l'api de flickr
- Mettre en place une testsuite fonctionnelle

# Conclusion

Ce projet à été plutôt intensif du fait de mon expérience nulle dans le web mais cela n'en à été que plus intéréssant et grisant. J'espère qu'il retiendra votre attention et suis à l'écoute de toute piste d'amélioration vis-à-vis de celui-ci.
