// import express framework
const express = require('express')
const app = express()
const port = 3000

// import NodeCache module
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

// get weather API key (same as ex1)
const wapiKey = process.env.WS_APIKEY;

const axios = require('axios');
const { query } = require('express');

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})

// index endpoint
app.get('/', (req, res) => {
  // res.send('Hello World!')
  res.status(200).json({status: 'ok'});
})

// endpoint to get lon and lat from a query (city name)
app.get('/geo', async (req, res) => {
  if (!req.query.search) {
    res.status(400).json({status: 'Missing \'search\' param'});
    return;
  }

  const cityName = req.query.search;

  // check cache
  const inCache = myCache.get(cityName);
  if (inCache !== undefined) {
    res.status(200).json({ data: inCache, status: "cached data" });
    return;
  }

  const params = {
    appid: wapiKey,
    q: cityName,
    limit: 1
  };

  const apiResponse = await axios.get('http://api.openweathermap.org/geo/1.0/direct', {params})
    .catch(error => {
        console.log(error);
    });

  if (apiResponse.status != "200") {
    res.status(200).json({ data: [], status: 'Call on API failed' });
    return;
  }

  // Cleans data, there shouldn't be a list since there is one elem
  // but in order to be coherent with front we will stay like that for now
  let qclean = [];
  const city = apiResponse.data[0];
  qclean.push({
    name: city.name,
    country: city.country,
    state: city.state,
    lon: city.lon,
    lat: city.lat
   });

  // save query in cache for 15 min
  myCache.set(cityName, qclean, 900);
 
  res.status(200).json({ data: qclean, status: "success" });
})

// gets the weather forecast for given query (lon, lat)
app.get('/weather', async (req, res) => {
  if (!req.query.lon) {
    res.status(400).json({status: 'Missing \'lon\' parameter'});
    return;
  }
  if (!req.query.lat) {
    res.status(400).json({status: 'Missing \'lat\' parameter'});
    return;
  }

  const lat = req.query.lat;
  const lon = req.query.lon;

  if (isNaN(parseFloat(lat))) {
    res.status(400).json({ status: 'Invalid \'lat\' parameter'});
    return;
  }
  if (isNaN(parseFloat(lon))) {
    res.status(400).json({ status: 'Invalid \'lon\' parameter'});
    return;
  }

  // check cache
  const inCache = myCache.get(lat+lon);
  if (inCache !== undefined) {
    res.status(200).json({ data: inCache, status: "cached data" });
    return;
  }

  const params = {
    appid: wapiKey,
    lon: lon,
    lat: lat,
    units: "metric",
    exclude: "minutely,hourly,alerts"
  }

  const apiResponse = await axios.get('https://api.openweathermap.org/data/2.5/onecall', {params})
    .catch(error => {
        console.log(error);
    });

  if (apiResponse.status != "200") {
    res.status(200).json({ data: [], status: 'Call on API failed' });
    return;
  }

  // TODO clean data
  let qclean = apiResponse.data;

  // save query in cache for 15 min
  myCache.set(lat+lon, qclean, 900);
 
  res.status(200).json({ data: qclean, status: "success" });
})